class Adder {
	public static void main() {
		adder(1, 2);
	}

	private void adder(int a, int b) {
		int numA = a;
		int numB = b;
		int sum = numA + numB;
		System.out.println(sum);
	}
}
